<?php

namespace App\Test\Controller;

use App\Entity\Customer;
use App\Repository\CustomerRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CustomerControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private CustomerRepository $repository;
    private string $path = '/customer/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->repository = static::getContainer()->get('doctrine')->getRepository(Customer::class);

        foreach ($this->repository->findAll() as $object) {
            $this->repository->remove($object, true);
        }
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Customer index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $originalNumObjectsInRepository = count($this->repository->findAll());

        $this->markTestIncomplete();
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'customer[id]' => 'Testing',
            'customer[customerNumber]' => 'Testing',
            'customer[title]' => 'Testing',
            'customer[phone]' => 'Testing',
            'customer[fax]' => 'Testing',
            'customer[clerk]' => 'Testing',
            'customer[termOfCredit]' => 'Testing',
            'customer[isLiableToTaxOnSales]' => 'Testing',
            'customer[updatedAt]' => 'Testing',
            'customer[createdAt]' => 'Testing',
        ]);

        self::assertResponseRedirects('/customer/');

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));
    }

    public function testShow(): void
    {
        $this->markTestIncomplete();
        $fixture = new Customer();
        $fixture->setId('My Title');
        $fixture->setCustomerNumber('My Title');
        $fixture->setTitle('My Title');
        $fixture->setPhone('My Title');
        $fixture->setFax('My Title');
        $fixture->setClerk('My Title');
        $fixture->setTermOfCredit('My Title');
        $fixture->setIsLiableToTaxOnSales('My Title');
        $fixture->setUpdatedAt('My Title');
        $fixture->setCreatedAt('My Title');

        $this->repository->save($fixture, true);

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Customer');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $this->markTestIncomplete();
        $fixture = new Customer();
        $fixture->setId('My Title');
        $fixture->setCustomerNumber('My Title');
        $fixture->setTitle('My Title');
        $fixture->setPhone('My Title');
        $fixture->setFax('My Title');
        $fixture->setClerk('My Title');
        $fixture->setTermOfCredit('My Title');
        $fixture->setIsLiableToTaxOnSales('My Title');
        $fixture->setUpdatedAt('My Title');
        $fixture->setCreatedAt('My Title');

        $this->repository->save($fixture, true);

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'customer[id]' => 'Something New',
            'customer[customerNumber]' => 'Something New',
            'customer[title]' => 'Something New',
            'customer[phone]' => 'Something New',
            'customer[fax]' => 'Something New',
            'customer[clerk]' => 'Something New',
            'customer[termOfCredit]' => 'Something New',
            'customer[isLiableToTaxOnSales]' => 'Something New',
            'customer[updatedAt]' => 'Something New',
            'customer[createdAt]' => 'Something New',
        ]);

        self::assertResponseRedirects('/customer/');

        $fixture = $this->repository->findAll();

        self::assertSame('Something New', $fixture[0]->getId());
        self::assertSame('Something New', $fixture[0]->getCustomerNumber());
        self::assertSame('Something New', $fixture[0]->getTitle());
        self::assertSame('Something New', $fixture[0]->getPhone());
        self::assertSame('Something New', $fixture[0]->getFax());
        self::assertSame('Something New', $fixture[0]->getClerk());
        self::assertSame('Something New', $fixture[0]->getTermOfCredit());
        self::assertSame('Something New', $fixture[0]->getIsLiableToTaxOnSales());
        self::assertSame('Something New', $fixture[0]->getUpdatedAt());
        self::assertSame('Something New', $fixture[0]->getCreatedAt());
    }

    public function testRemove(): void
    {
        $this->markTestIncomplete();

        $originalNumObjectsInRepository = count($this->repository->findAll());

        $fixture = new Customer();
        $fixture->setId('My Title');
        $fixture->setCustomerNumber('My Title');
        $fixture->setTitle('My Title');
        $fixture->setPhone('My Title');
        $fixture->setFax('My Title');
        $fixture->setClerk('My Title');
        $fixture->setTermOfCredit('My Title');
        $fixture->setIsLiableToTaxOnSales('My Title');
        $fixture->setUpdatedAt('My Title');
        $fixture->setCreatedAt('My Title');

        $this->repository->save($fixture, true);

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertSame($originalNumObjectsInRepository, count($this->repository->findAll()));
        self::assertResponseRedirects('/customer/');
    }
}
