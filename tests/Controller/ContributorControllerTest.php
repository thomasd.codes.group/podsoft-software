<?php

namespace App\Test\Controller;

use App\Entity\Contributor;
use App\Repository\ContributorRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ContributorControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private ContributorRepository $repository;
    private string $path = '/contributor/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->repository = static::getContainer()->get('doctrine')->getRepository(Contributor::class);

        foreach ($this->repository->findAll() as $object) {
            $this->repository->remove($object, true);
        }
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Contributor index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $originalNumObjectsInRepository = count($this->repository->findAll());

        $this->markTestIncomplete();
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'contributor[id]' => 'Testing',
            'contributor[firstName]' => 'Testing',
            'contributor[familyName]' => 'Testing',
            'contributor[title]' => 'Testing',
            'contributor[updatedAt]' => 'Testing',
            'contributor[createdAt]' => 'Testing',
            'contributor[gender]' => 'Testing',
        ]);

        self::assertResponseRedirects('/contributor/');

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));
    }

    public function testShow(): void
    {
        $this->markTestIncomplete();
        $fixture = new Contributor();
        $fixture->setId('My Title');
        $fixture->setFirstName('My Title');
        $fixture->setFamilyName('My Title');
        $fixture->setTitle('My Title');
        $fixture->setUpdatedAt('My Title');
        $fixture->setCreatedAt('My Title');
        $fixture->setGender('My Title');

        $this->repository->save($fixture, true);

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Contributor');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $this->markTestIncomplete();
        $fixture = new Contributor();
        $fixture->setId('My Title');
        $fixture->setFirstName('My Title');
        $fixture->setFamilyName('My Title');
        $fixture->setTitle('My Title');
        $fixture->setUpdatedAt('My Title');
        $fixture->setCreatedAt('My Title');
        $fixture->setGender('My Title');

        $this->repository->save($fixture, true);

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'contributor[id]' => 'Something New',
            'contributor[firstName]' => 'Something New',
            'contributor[familyName]' => 'Something New',
            'contributor[title]' => 'Something New',
            'contributor[updatedAt]' => 'Something New',
            'contributor[createdAt]' => 'Something New',
            'contributor[gender]' => 'Something New',
        ]);

        self::assertResponseRedirects('/contributor/');

        $fixture = $this->repository->findAll();

        self::assertSame('Something New', $fixture[0]->getId());
        self::assertSame('Something New', $fixture[0]->getFirstName());
        self::assertSame('Something New', $fixture[0]->getFamilyName());
        self::assertSame('Something New', $fixture[0]->getTitle());
        self::assertSame('Something New', $fixture[0]->getUpdatedAt());
        self::assertSame('Something New', $fixture[0]->getCreatedAt());
        self::assertSame('Something New', $fixture[0]->getGender());
    }

    public function testRemove(): void
    {
        $this->markTestIncomplete();

        $originalNumObjectsInRepository = count($this->repository->findAll());

        $fixture = new Contributor();
        $fixture->setId('My Title');
        $fixture->setFirstName('My Title');
        $fixture->setFamilyName('My Title');
        $fixture->setTitle('My Title');
        $fixture->setUpdatedAt('My Title');
        $fixture->setCreatedAt('My Title');
        $fixture->setGender('My Title');

        $this->repository->save($fixture, true);

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertSame($originalNumObjectsInRepository, count($this->repository->findAll()));
        self::assertResponseRedirects('/contributor/');
    }
}
