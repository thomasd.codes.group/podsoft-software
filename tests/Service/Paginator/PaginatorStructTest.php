<?php

namespace App\Test\Service\Paginator;

use App\Service\Paginator\PaginatorStruct;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class PaginatorStructTest extends TestCase
{
    public function testFactoryInitialsPaginatorStruct(): void
    {
        $paginatorStruct = PaginatorStruct::createWithRequestAndTotalCount(new Request(), 123);

        $this->assertTrue($paginatorStruct instanceof PaginatorStruct);
    }

    public function testGivesBackCurrentPage(): void
    {
        $paginatorStruct = PaginatorStruct::createWithRequestAndTotalCount(new Request(query: ['page' => 3]), 123);

        $this->assertEquals(3, $paginatorStruct->getCurrentPage());
    }

    public function testGivesBackCorrectNumberOfPages(): void
    {
        $paginatorStruct = PaginatorStruct::createWithRequestAndTotalCount(new Request(query: ['page' => 2]), 123);

        //Standard Limit = 50 -> 123 / 50 = 2 R 23 -> 3 Pages
        $this->assertEquals(3, $paginatorStruct->getNumberOfPages());
    }

    public function testGivesBackCorrectOffsetForPage2(): void
    {
        $paginatorStruct = PaginatorStruct::createWithRequestAndTotalCount(new Request(query: ['page' => 2]), 123);

        //Standard Limit = 50 -> Seite 2 = Offset => 100
        $this->assertEquals(50, $paginatorStruct->getOffset());
    }

    public function testGivesBackCorrectOffsetForPage3(): void
    {
        $paginatorStruct = PaginatorStruct::createWithRequestAndTotalCount(new Request(query: ['page' => 3]), 123);

        //Standard Limit = 50 -> Seite 2 = Offset => 100
        $this->assertEquals(100, $paginatorStruct->getOffset());
    }
}
