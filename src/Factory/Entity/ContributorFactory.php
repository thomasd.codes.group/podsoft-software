<?php

declare(strict_types=1);

namespace App\Factory\Entity;

use App\Entity\Contributor;

class ContributorFactory
{
    protected const DATETIME_FORMAT = 'd.m.y-H:i';

    public function createWithCsvData(array $data): Contributor
    {
        $contributor = (new Contributor())
            ->setLegacyId((int)$data[0])
            ->setTitle($data[2])
        ;

        if (!empty($data[3])) {
            $contributor->setSubtitle($data[3]);
        }

        $createdAt = \DateTimeImmutable::createFromFormat(self::DATETIME_FORMAT, $data[34], new \DateTimeZone('EUROPE/BERLIN'));
        if ($createdAt) {
            $contributor->setCreatedAt($createdAt);
        }

        $updatedAt = \DateTimeImmutable::createFromFormat(self::DATETIME_FORMAT, $data[35], new \DateTimeZone('EUROPE/BERLIN'));
        if ($updatedAt) {
            $contributor->setUpdatedAt($updatedAt);
        }

        return $contributor;
    }
}