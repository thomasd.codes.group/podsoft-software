<?php

namespace App\Form;

use App\Entity\Customer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('customerNumber', NumberType::class, [
                'label' => 'Verkehrs-/Kundennummer'
            ])
            ->add('title', TextType::class, [
                'label' => 'Name'
            ])
            ->add('phone', TelType::class, [
                'label' => 'Telefonnummer',
                'required' => false
            ])
            ->add('fax', TelType::class, [
                'label' => 'Faxnummer',
                'required' => false
            ])
            ->add('clerk', TextType::class, [
                'label' => 'Ansprechpartner',
                'required' => false
            ])
            ->add('termOfCredit', NumberType::class, [
                'label' => 'Zahlungsziel'
            ])
            ->add('isLiableToTaxOnSales', CheckboxType::class, [
                'label' => 'Steuerpflichtig'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Customer::class,
        ]);
    }
}
