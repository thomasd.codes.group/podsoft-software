<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;

class GenericImportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('importFile', FileType::class, [
                'label' => 'Importdatei',
                'help' => 'Bitte CSV zum importieren auswählen.'
            ])
        ;
    }
}
