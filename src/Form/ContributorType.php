<?php

namespace App\Form;

use App\Entity\Contributor;
use App\Entity\Gender;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContributorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('gender', EntityType::class, [
                'label' => 'Anrede',
                'class' => Gender::class,
                'choice_label' => 'name',
            ])
            ->add('title', TextType::class, [
                'label' => 'Titel',
                'required' => false,
            ])
            ->add('firstName', TextType::class, [
                'label' => 'Vorname',
                'required' => false,
            ])
            ->add('familyName', TextType::class, [
                'label' => 'Nachname',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contributor::class,
        ]);
    }
}
