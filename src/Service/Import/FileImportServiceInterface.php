<?php

namespace App\Service\Import;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface FileImportServiceInterface
{
    public function setFile(UploadedFile $file): void;

    public function importFile(): FileImportResultStruct;
}