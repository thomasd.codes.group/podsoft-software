<?php

declare(strict_types=1);

namespace App\Service\Import;

class FileImportResultStruct
{
    protected int $numberOfDatasets = 0;
    protected int $numberOfErrors = 0;
    protected array $listOfErrors = [];

    public function getNumberOfDatasets(): int
    {
        return $this->numberOfDatasets;
    }

    public function setNumberOfDatasets(int $numberOfDatasets): void
    {
        $this->numberOfDatasets = $numberOfDatasets;
    }

    public function getNumberOfErrors(): int
    {
        return $this->numberOfErrors;
    }

    public function setNumberOfErrors(int $numberOfErrors): void
    {
        $this->numberOfErrors = $numberOfErrors;
    }

    /**
     * @return string[]
     */
    public function getListOfErrors(): array
    {
        return $this->listOfErrors;
    }

    public function setListOfErrors(array $listOfErrors): void
    {
        $this->listOfErrors = $listOfErrors;
    }

    public function addError(string $error): void
    {
        $this->numberOfErrors++;
        $this->listOfErrors[] = $error;
    }
}