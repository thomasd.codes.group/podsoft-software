<?php

declare(strict_types=1);

namespace App\Service\Import;

use App\Factory\Entity\ContributorFactory;
use App\Repository\ContributorRepository;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ContributorImportService implements FileImportServiceInterface
{
    protected ?UploadedFile $file = null;

    public function __construct(
        protected readonly ContributorRepository $contributorRepository,
        protected readonly ContributorFactory $contributorFactory
    )
    {
    }

    public function setFile(UploadedFile $file): void
    {
        $this->file = $file;
    }

    public function importFile(): FileImportResultStruct
    {
        $fileImportStruct = new FileImportResultStruct();
        if (($handle = fopen($this->file->getPathname(), 'r')) !== FALSE) {
            while (($data = fgetcsv($handle, null, ",")) !== FALSE) {
                if (is_array($data)) {
                    $contributor = $this->contributorFactory->createWithCsvData($data);

                    try {
                        $this->contributorRepository->save($contributor, true);
                        $fileImportStruct->setNumberOfDatasets($fileImportStruct->getNumberOfDatasets() + 1);
                    } catch (\Exception $e) {
                        $fileImportStruct->addError($e->getMessage());
                    }
                }
            }

        }
        return $fileImportStruct;
    }
}