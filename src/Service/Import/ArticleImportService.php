<?php

declare(strict_types=1);

namespace App\Service\Import;

use App\Factory\Entity\ArticleFactory;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ArticleImportService implements FileImportServiceInterface
{
    protected ?UploadedFile $file = null;

    public function __construct(
        protected readonly ArticleRepository $articleRepository,
        protected readonly ArticleFactory    $articleFactory
    )
    {
    }

    public function setFile(UploadedFile $file): void
    {
        $this->file = $file;
    }

    public function importFile(): FileImportResultStruct
    {
        $fileImportStruct = new FileImportResultStruct();
        if (($handle = fopen($this->file->getPathname(), 'r')) !== FALSE) {
            while (($data = fgetcsv($handle, null, ",")) !== FALSE) {
                if (is_array($data)) {
                    $article = $this->articleFactory->createWithCsvData($data);

                    try {
                        $this->articleRepository->save($article, true);
                        $fileImportStruct->setNumberOfDatasets($fileImportStruct->getNumberOfDatasets() + 1);
                    } catch (\Exception $e) {
                        $fileImportStruct->addError($e->getMessage());
                    }
                }
            }

        }
        return $fileImportStruct;
    }
}