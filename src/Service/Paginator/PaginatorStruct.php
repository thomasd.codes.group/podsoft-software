<?php

declare(strict_types=1);

namespace App\Service\Paginator;

use Symfony\Component\HttpFoundation\Request;

class PaginatorStruct
{
    protected int $totalCount;
    protected int $numberOfPages;
    protected int $currentPage;
    protected int $limit = 50;
    protected int $offset = 0;

    public static function createWithRequestAndTotalCount(Request $request, int $totalCount): PaginatorStruct
    {
        $instance = new self(
            totalCount: $totalCount,
            currentPage: (int)$request->get('page', 1)
        );

        $instance->updateOffset();

        return $instance;
    }

    public function __construct(
        int $totalCount = 1,
        int $currentPage = 1
    )
    {
        $this->totalCount = $totalCount;
        $this->currentPage = $currentPage;
        $this->numberOfPages = (int)ceil($totalCount/self::getLimit());
    }

    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

    public function setTotalCount(int $totalCount): self
    {
        $this->totalCount = $totalCount;
        return $this;
    }

    public function getNumberOfPages(): int
    {
        return $this->numberOfPages;
    }

    public function setNumberOfPages(int $numberOfPages): self
    {
        $this->numberOfPages = $numberOfPages;
        return $this;
    }

    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    public function setCurrentPage(int $currentPage): self
    {
        $this->currentPage = $currentPage;
        return $this;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function setLimit(int $limit): self
    {
        $this->limit = $limit;
        return $this;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function setOffset(int $offset): self
    {
        $this->offset = $offset;
        return $this;
    }

    protected function updateOffset(): void
    {
        $this->setOffset(($this->currentPage - 1) * $this->limit);
    }
}