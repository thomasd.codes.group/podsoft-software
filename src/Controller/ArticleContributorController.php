<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/article/contributor', name: 'app_article_contributor_')]
class ArticleContributorController extends AbstractController
{
    #[Route(path: '/{id}', name: 'index')]
    public function index(Article $article): Response
    {
        return $this->render('article/contributor/index.html.twig', [
            'article' => $article,
        ]);
    }
}
