<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\GenericImportType;
use App\Service\Import\FileImportServiceInterface;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/app/article', name: 'app_article_')]
class ArticleImportController extends AbstractImportController
{
    protected const TEMPLATE = 'article/import/form.html.twig';
    protected const FORM_CLASS = GenericImportType::class;

    public function __construct(protected readonly FileImportServiceInterface $articleImportService)
    {
    }
}