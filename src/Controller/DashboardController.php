<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/app', name: 'app_dashboard_')]
class DashboardController extends AbstractController
{
    #[Route('/dashboard', name: 'index')]
    public function index(ArticleRepository $articleRepository): Response
    {
        return $this->render('dashboard/index.html.twig', [
            'lastArticles' => $articleRepository->findBy([], ['createdAt' => 'DESC'], 5),
        ]);
    }
}
