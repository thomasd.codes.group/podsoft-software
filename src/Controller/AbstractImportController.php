<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\Import\FileImportServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

abstract class AbstractImportController extends AbstractController
{
    protected const FORM_CLASS = '';
    protected const TEMPLATE = '';

    protected readonly FileImportServiceInterface $importService;

    #[Route(path: '/import', name: 'import', methods: ['GET', 'POST'])]
    public function import(Request $request): Response
    {
        $form = $this->createForm(static::FORM_CLASS);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form['importFile']->getData();
            $this->importService->setFile($file);
            $resultStruct = $this->importService->importFile();

            if ($resultStruct->getNumberOfErrors() > 0) {
                $this->addFlash('danger', sprintf('%d Datensätze waren fehlerhaft', $resultStruct->getNumberOfErrors()));
            }

            $this->addFlash('success', sprintf('%d Datensätze wurden eingefügt.', $resultStruct->getNumberOfDatasets()));
        }

        return $this->render(static::TEMPLATE, [
            'form' => $form
        ]);
    }
}