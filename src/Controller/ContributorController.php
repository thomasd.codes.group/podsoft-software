<?php

namespace App\Controller;

use App\Entity\Contributor;
use App\Form\ContributorType;
use App\Repository\ContributorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/contributor', name: 'app_contributor_')]
class ContributorController extends AbstractController
{
    public function __construct(protected readonly ContributorRepository $contributorRepository)
    {
    }

    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(): Response
    {
        return $this->render('contributor/index.html.twig', [
            'contributors' => $this->contributorRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $contributor = new Contributor();
        $form = $this->createForm(ContributorType::class, $contributor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->contributorRepository->save($contributor, true);

            $this->addFlash('success', 'Erfolgreich angelegt!');
            return $this->redirectToRoute('app_contributor_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('contributor/new.html.twig', [
            'contributor' => $contributor,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'show', methods: ['GET'])]
    public function show(Contributor $contributor): Response
    {
        return $this->render('contributor/show.html.twig', [
            'contributor' => $contributor,
        ]);
    }

    #[Route('/{id}/edit', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Contributor $contributor, ContributorRepository $contributorRepository): Response
    {
        $form = $this->createForm(ContributorType::class, $contributor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contributorRepository->save($contributor, true);

            $this->addFlash('success', 'Erfolgreich editiert!');
            return $this->redirectToRoute('app_contributor_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('contributor/edit.html.twig', [
            'contributor' => $contributor,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, Contributor $contributor, ContributorRepository $contributorRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$contributor->getId(), $request->request->get('_token'))) {
            $contributorRepository->remove($contributor, true);
        }

        return $this->redirectToRoute('app_contributor_index', [], Response::HTTP_SEE_OTHER);
    }
}
