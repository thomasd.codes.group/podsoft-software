<?php

namespace App\Entity;

use App\Entity\Traits\CreatedAtTrait;
use App\Entity\Traits\UpdatedAtTrait;
use App\Repository\CustomerRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: CustomerRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Customer
{
    use UpdatedAtTrait;
    use CreatedAtTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    private Uuid $id;

    #[ORM\Column(length: 25, nullable: true)]
    private ?string $customerNumber = null;

    #[ORM\Column(length: 150)]
    private ?string $title = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $phone = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $fax = null;

    #[ORM\Column(length: 150, nullable: true)]
    private ?string $clerk = null;

    #[ORM\Column(nullable: true)]
    private ?int $termOfCredit = null;

    #[ORM\Column]
    private ?bool $isLiableToTaxOnSales = true;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getCustomerNumber(): ?string
    {
        return $this->customerNumber;
    }

    public function setCustomerNumber(string $customerNumber): self
    {
        $this->customerNumber = $customerNumber;
        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;
        return $this;
    }

    public function getFax(): ?string
    {
        return $this->fax;
    }

    public function setFax(string $fax): self
    {
        $this->fax = $fax;
        return $this;
    }

    public function getClerk(): ?string
    {
        return $this->clerk;
    }

    public function setClerk(string $clerk): self
    {
        $this->clerk = $clerk;
        return $this;
    }

    public function getTermOfCredit(): ?int
    {
        return $this->termOfCredit;
    }

    public function setTermOfCredit(int $termOfCredit): self
    {
        $this->termOfCredit = $termOfCredit;
        return $this;
    }

    public function isLiableToTaxOnSales(): bool
    {
        return $this->isLiableToTaxOnSales;
    }

    public function setIsLiableToTaxOnSales(bool $isLiableToTaxOnSales): self
    {
        $this->isLiableToTaxOnSales = $isLiableToTaxOnSales;
        return $this;
    }
}
