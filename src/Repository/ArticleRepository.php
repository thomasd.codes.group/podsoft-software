<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @extends ServiceEntityRepository<Article>
 *
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    protected ?SessionInterface $session = null;

    public function __construct(ManagerRegistry $registry, RequestStack $requestStack)
    {
        parent::__construct($registry, Article::class);
        $this->session = $requestStack->getSession();
    }

    public function save(Article $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Article $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function loadListing(int $limit, int $offset): array
    {
        if ($this->session) {
            $sessionKey = sprintf('articles_%d_%d', $limit, $offset);
            if (!$this->session->has($sessionKey)) {
                $this->session->set($sessionKey, $this->findBy([], ['legacyId' => 'ASC'], $limit, $offset));
            }

            return $this->session->get($sessionKey);
        }

        return $this->findBy([], ['legacyId' => 'ASC'], $limit, $offset);
    }

    public function getCount(): int
    {
        $qb = $this->createQueryBuilder('a');
        $qb->select($qb->expr()->count('a'));
        $query = $qb->getQuery();

        if ($this->session) {
            $sessionKey = 'articles_count';

            if (!$this->session->has($sessionKey)) {
                $this->session->set($sessionKey, $query->getSingleScalarResult());
            }

            return $this->session->get($sessionKey);
        }

        return $query->getSingleScalarResult();
    }
}
